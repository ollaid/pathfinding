import pygame
import math
from queue import PriorityQueue

width = 600
win = pygame.display.set_mode((width, width))
pygame.display.set_caption("A* pathfinding algorithm")

RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
PURPLE = (128, 0, 128)
ORANGE = (255, 165, 0)
GREY = (128, 128, 128)
TURQUOISE = (64, 224, 208)

class Square:
    def __init__(self, row, col, width, total_rows):
        self.row = row
        self.col = col
        self.x = row * width
        self.y = col * width
        self.color = WHITE
        self.neighs = []
        self.width = width
        self.total_rows = total_rows

    def get_pos(self):
        return self.row, self.col

    def is_closed(self):
        return self.color == RED

    def is_open(self):
        return self.color == GREEN

    def is_border(self):
        return self.color == BLACK

    def is_start(self):
        return self.color == ORANGE

    def is_end(self):
        return self.color == TURQUOISE

    def reset(self):
        self.color = WHITE

    def make_closed(self):
        self.color = RED

    def make_open(self):
        self.color = GREEN

    def make_border(self):
        self.color = BLACK

    def make_end(self):
        self.color = TURQUOISE

    def make_path(self):
        self.color = PURPLE

    def make_start(self):
        self.color = ORANGE

    def make_barrier(self):
        self.color = BLACK

    def draw(self, window):
        pygame.draw.rect(window, self.color, (self.x, self.y, self.width, self.width))

    def update_neighs(self, grid):
        self.neighs = []
        if self.row < self.total_rows - 1 and not grid[self.row + 1][self.col].is_border():
            self.neighs.append(grid[self.row + 1][self.col]) # down
        if self.row > 0 and not grid[self.row - 1][self.col].is_border():
            self.neighs.append(grid[self.row - 1][self.col]) # up
        if self.col < self.total_rows - 1 and not grid[self.row][self.col + 1].is_border():
            self.neighs.append(grid[self.row][self.col + 1]) # right
        if self.col > 0 and not grid[self.row][self.col - 1].is_border():
            self.neighs.append(grid[self.row][self.col - 1]) # left

    def __lt__(self, other):
        return False

def heuristic(pos1, pos2):
    x1, y1 = pos1
    x2, y2 = pos2
    return abs(x1 - x2) + abs(y1 - y2)

def reconstruct_path(came_from, current, draw):
    while current in came_from:
        current = came_from[current]
        current.make_path()
        draw()
        last_current = current
    last_current.make_start()

def algorithm(draw, grid, start, end):
    count = 0
    open_set = PriorityQueue()
    open_set.put((0, count, start))
    came_from = {}
    g_score = {square: float("inf") for row in grid for square in row}
    g_score[start] = 0
    f_score = {square: float("inf") for row in grid for square in row}
    f_score[start] = heuristic(start.get_pos(), end.get_pos())

    open_set_hash = {start} # checks if the thing is in priorityQueue

    while not open_set.empty():
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit
        current = open_set.get()[2] # gets minimum element
        open_set_hash.remove(current)

        if current == end:
            reconstruct_path(came_from, end, draw)
            end.make_end()
            return True

        for neigh in current.neighs:
            temp_g_score = g_score[current] + 1
            if temp_g_score < g_score[neigh]:
                came_from[neigh] = current
                g_score[neigh] = temp_g_score
                f_score[neigh] = temp_g_score + heuristic(neigh.get_pos(), end.get_pos())
                if neigh not in open_set_hash:
                    count += 1
                    open_set.put((f_score[neigh], count, neigh))
                    open_set_hash.add(neigh)
                    neigh.make_open()
        draw()

        if current != start:
            current.make_closed()
    return False

def build_grid(rows, width):
    grid = []
    gap = width // rows
    for i in range(rows):
        grid.append([])
        for j in range(rows):
            square = Square(i, j, gap, rows)
            grid[i].append(square)

    return grid

def draw_grid_lines(win, rows, width):
    gap = width // rows
    for i in range(rows): # horizontal lines
        pygame.draw.line(win, GREY, (0, i * gap), (width, i * gap))
        for j in range(rows): # vertical lines
            pygame.draw.line(win, GREY, (j * gap, 0), (j * gap, width))

def draw(window, grid, rows, width):
    window.fill(WHITE)

    for row in grid:
        for square in row:
            square.draw(window)

    draw_grid_lines(window, rows, width)
    pygame.display.update()

def get_clicked_mouse_positsion(mouse_pos, rows, width):
    gap = width // rows
    y, x = mouse_pos
    row = y // gap
    col = x // gap
    return row, col

def main(window, width):
    total_rows = 50
    grid = build_grid(total_rows, width)
    
    start_pos = None
    end_pos = None
    game_running = True

    while game_running:
        draw(window, grid, total_rows, width)
        for event in pygame.event.get():
            if (event.type == pygame.QUIT):
                game_running = False
            if pygame.mouse.get_pressed()[0]: # left button
                positsion = pygame.mouse.get_pos()
                row, col = get_clicked_mouse_positsion(positsion, total_rows, width)
                square = grid[row][col]
                if not start_pos and square != end_pos:
                    start_pos = square
                    start_pos.make_start()
                elif not end_pos and square != start_pos:
                    end_pos = square
                    end_pos.make_end()
                elif square != end_pos and square != start_pos:
                    square.make_barrier()
            elif pygame.mouse.get_pressed()[2]: # right button
                positsion = pygame.mouse.get_pos()
                row, col = get_clicked_mouse_positsion(positsion, total_rows, width)
                square = grid[row][col]
                square.reset()
                if square == start_pos:
                    start_pos = None
                elif square == end_pos:
                    end_pos = None
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE and start_pos and end_pos:
                    for row in grid:
                        for square in row:
                            square.update_neighs(grid)
                    algorithm(lambda: draw(win, grid, total_rows, width), grid, start_pos, end_pos)
                if event.key == pygame.K_c:
                    start_pos = None
                    end_pos = None
                    grid = build_grid(total_rows, width)

    pygame.quit()

main(win, width)